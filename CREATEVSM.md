# How to create TAM-Arkiv VSM data

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:bildnummer", "type": "string", "utdatert": false, "beskrivelse": "bildnummer metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq  

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:motiv", "type": "string", "utdatert": false, "beskrivelse": "motiv metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:plats", "type": "string", "utdatert": false, "beskrivelse": "plats metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq  

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:ar", "type": "string", "utdatert": false, "beskrivelse": "ar metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq  

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:negativ", "type": "string", "utdatert": false, "beskrivelse": " metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:publikation", "type": "string", "utdatert": false, "beskrivelse": "publikation metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:bildrattigheter", "type": "string", "utdatert": false, "beskrivelse": "bildrattigheter metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq  

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:placering", "type": "string", "utdatert": false, "beskrivelse": "placering metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:index", "type": "string", "utdatert": false, "beskrivelse": "index metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq    

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:anmarkningar", "type": "string", "utdatert": false, "beskrivelse": "anmarkningar metadata", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq  

curl  --header 'Content-Type:application/vnd.noark5+json'  --header 'Accept:application/vnd.noark5+json' --header "Authorization: Bearer TOKEN" --data '{"navn": "tab:tokens", "type": "string", "utdatert": false, "beskrivelse": "used to aid indexing and search. Not part of database", "kilde": "https://some/where/with/explanation"}' -X POST http://localhost:8092/noark5v5/api/metadata/ny-virksomhetsspesifikkeMetadata  | jq
