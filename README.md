# pictor2Noark5

A simple application that turns the contents of the Pictor database to an arkivstruktur.xml file. The application is a maven application so all project
dependencies are handled in pom.xml.

## Building the application

Pictor2Noark5 is developed with Java 17 and Apache Maven 3.11.0. The application can be built by running the following command in the root of the source:

    mvn clean package

Note: The Java version was bumped from 11 to 17, so it will compile and run under Java 11. Future developments may result in Java 17 functionality.

## Running the application
Once the application has been built it can be run using:

      mvn -q exec:java -Dexec.arguments="-filePictor.mdb"

*Note: The lack of space in : -filePictor.mdb*

## Structure 

There are two tables in the database:

 1. TabArkiv
 2. TabBild

TabArkiv has rows and seems to describe various archives. There is no relationship
between TabArkiv and TabBild. TabBild contains metadata about all the pictures. The
actual pictures are stored external to the Access database.

The following are a list of columns in the Access file:

	 Arkivbildare (TEXT)
	 Bildnummer (TEXT)
	 Negativ (BOOLEAN)
	 Plats (TEXT)
	 Ar (TEXT)
	 Motiv (MEMO)
	 Publikation (TEXT)
	 Bildrattigheter (TEXT)
	 Anmarkningar (TEXT)
	 Placering (TEXT)
	 Index (LONG)
    
