package no.oslomet.abi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.healthmarketscience.jackcess.*;
import no.oslomet.abi.model.noark.VSM;
import no.oslomet.abi.model.noark.*;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.*;

import static java.lang.System.out;
import static java.io.File.separator;

import static no.oslomet.abi.utils.Constants.*;

public class MDBReader {

    private final Database db;
    private final XMLWriter xmlWriter;
    private final XSDWriter xsdWriter;
    private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    private final Map<String, Map<String, Set<Row>>> tableContents =
            new HashMap<>();

    public MDBReader(String file, XMLWriter xmlWriter, XSDWriter xsdWriter) throws IOException {
        db = DatabaseBuilder.open(new java.io.File(file));
        this.xmlWriter = xmlWriter;
        this.xsdWriter = xsdWriter;
    }

    public void printColumns() throws IOException {
        Table tableBild = db.getTable("TabBild");
        out.println(tableBild.getName());
        for (Column column : tableBild.getColumns()) {
            String columnName = column.getName();
            out.println("\tColumn " + columnName + "(" +
                    column.getType() + ")");
        }
    }

    private void initialise() throws XMLStreamException, IOException {

        xsdWriter.writeVSMXSD();

        Fonds fonds =
                mapper.readValue(new java.io.File(
                                Objects.requireNonNull(getClass().getClassLoader()
                                        .getResource("fonds.yml")).getFile()),
                        Fonds.class);
        xmlWriter.openFonds();
        xmlWriter.writeFonds(fonds);
        FondsCreator fondsCreator = mapper.readValue(
                new java.io.File(Objects.requireNonNull(
                        getClass().getClassLoader()
                                .getResource("fondsCreator.yml")).getFile()),
                FondsCreator.class);
        xmlWriter.writeFondsCreator(fondsCreator);
    }

    private Series startSeries() throws XMLStreamException, IOException {
        Series series = mapper.readValue(new java.io.File(
                Objects.requireNonNull(getClass().getClassLoader()
                        .getResource("series.yml")).getFile()), Series.class);
        xmlWriter.openComplexType(SERIES);
        return series;
    }

    private void finalise() throws XMLStreamException {
        xmlWriter.closeFonds();
    }

    public void processData() throws IOException, XMLStreamException {
        initialise();

        Table tableBild = db.getTable("TabBild");
        out.println("Processing table [" + tableBild.getName() + "]");

        for (Column column : tableBild.getColumns()) {
            String columnName = column.getName();
            out.println("\tColumn " + columnName + "(" +
                    column.getType() + ")");
        }

        @SuppressWarnings("WrapperTypeMayBePrimitive")
        Integer recordCounter = 0;

        for (Row row : tableBild) {

            String key = row.getString("Arkivbildare");
            String year = row.getString("Ar");
            Map<String, Set<Row>> seriesMap;
            if (tableContents.containsKey(key)) {
                seriesMap = tableContents.get(key);
            } else {
                seriesMap = new HashMap<>();
                tableContents.put(key, seriesMap);
            }
            if (year == null || year.equals("?") || year.equals("(?)")) {
                year = "Ukjent";
            }
            Set<Row> yearMap;
            if (seriesMap.containsKey(year)) {
                yearMap = seriesMap.get(year);
            } else {
                yearMap = new HashSet<>();
                seriesMap.put(year, yearMap);
            }
            yearMap.add(row);
        }

        for (Map.Entry<String, Map<String, Set<Row>>> mapEntry :
                tableContents.entrySet()) {
            out.println(mapEntry.getKey());
            Series series = startSeries();
            series.setTitle(mapEntry.getKey());
            xmlWriter.writeSeries(series);
            Map<String, Set<Row>> yearMap = mapEntry.getValue();
            for (Map.Entry<String, Set<Row>> yearMapEntry :
                    yearMap.entrySet()) {
                File file = mapper.readValue(
                        new java.io.File(Objects.requireNonNull(
                                getClass().getClassLoader()
                                        .getResource("file.yml"))
                                .getFile()),
                        File.class);
                file.setFileId(yearMapEntry.getKey());
                file.setTitle(yearMapEntry.getKey());
                xmlWriter.openComplexType(FILE);
                xmlWriter.writeFile(file);
                Set<Row> rows = yearMapEntry.getValue();

                for (Row row : rows) {
                    xmlWriter.openComplexType(RECORD);
                    NoarkRecord noarkRecord = mapper.readValue(
                            new java.io.File(Objects.requireNonNull(
                                    getClass().getClassLoader()
                                            .getResource("record.yml"))
                                    .getFile()), NoarkRecord.class);
                    VSM vsm = new VSM();

                    vsm.setArkivbildare(row.getString("Arkivbildare"));
                    vsm.setBildnummer(row.getString("Bildnummer"));
                    vsm.setMotiv(row.getString("Motiv"));
                    vsm.setPlats(row.getString("Plats"));
                    vsm.setAr(row.getString("Ar"));
                    vsm.setNegativ(row.getBoolean("Negativ"));
                    vsm.setPublikation(row.getString("Publikation"));
                    vsm.setBildrattigheter(row.getString("Bildrattigheter"));
                    vsm.setAnmarkningar(row.getString("Anmarkningar"));
                    vsm.setPlacering(row.getString("Placering"));
                    vsm.setIndex(row.getInt("Index"));
                    recordCounter += 1;

                    noarkRecord.setRecordId(recordCounter.toString());
                    noarkRecord.setTitle(row.getString("Anmarkningar"));
                    xmlWriter.writeRecordPart1(noarkRecord);
                    java.io.File picture = getFile(
                            row.getString("Bildnummer"));

                    if (picture != null) {
                        DocumentDescription documentDescription =
                                mapper.readValue(
                                        new java.io.File(Objects.requireNonNull(
                                                getClass().getClassLoader()
                                                        .getResource(
                                                                "documentDescription.yml"))
                                                .getFile()),
                                        DocumentDescription.class);
                        documentDescription.setTitle(xmlWriter.combineValues(vsm));
                        xmlWriter.openComplexType(DOCUMENT_DESCRIPTION);
                        xmlWriter.writeDocumentDescription(documentDescription);

                        // Create Document Object for small file
                        DocumentObject smallDocumentObject = mapper.readValue(
                                new java.io.File(Objects.requireNonNull(
                                        getClass().getClassLoader()
                                                .getResource(
                                                        "documentObject.yml"))
                                        .getFile()),
                                DocumentObject.class);

                        xmlWriter.openComplexType(DOCUMENT_OBJECT);
                        smallDocumentObject.setChecksum(calculateChecksum(picture));
                        smallDocumentObject.setFileSize(getFileLength(picture));
                        smallDocumentObject.setReferenceDocumentFile(
                                DOCUMENT_DIRECTORY + separator + "small" + separator + row.getString("Bildnummer") +
                                        ".jpg");
                        xmlWriter.writeDocumentObject(smallDocumentObject);
                        xmlWriter.closeComplexType(); // smallDocumentObject

                        // Create Document Object for small file
                        DocumentObject largeDocumentObject = mapper.readValue(
                                new java.io.File(Objects.requireNonNull(
                                        getClass().getClassLoader()
                                                .getResource(
                                                        "documentObject.yml"))
                                        .getFile()),
                                DocumentObject.class);

                        xmlWriter.openComplexType(DOCUMENT_OBJECT);
                        largeDocumentObject.setChecksum(calculateChecksum(picture));
                        largeDocumentObject.setFileSize(getFileLength(picture));
                        largeDocumentObject.setReferenceDocumentFile(
                                DOCUMENT_DIRECTORY + separator + "large" + separator + row.getString("Bildnummer") +
                                        ".jpg");
                        xmlWriter.writeDocumentObject(largeDocumentObject);
                        xmlWriter.closeComplexType(); // largeDocumentObject
                        xmlWriter.closeComplexType(); // DocumentDescription
                    }
                    xmlWriter.writeRecordPart2(noarkRecord);
                    xmlWriter.writeVSM(vsm);
                    xmlWriter.closeComplexType(); // Record
                }
                xmlWriter.closeComplexType(); // File
            }
            xmlWriter.closeComplexType();
        }
        finalise();
    }

    private java.io.File getFile(String pictureId) {
        out.println("Looking for " + pictureId);
        String filePath = "pictures" + java.io.File.separator + pictureId +
                ".jpg";
        URL fileURL = getClass().getClassLoader().getResource(filePath);
        if (fileURL != null) {
            return new java.io.File(fileURL.getFile());
        }
        return null;
    }

    private Long getFileLength(java.io.File file) {
        return file.length();
    }

    private String calculateChecksum(java.io.File file)
            throws IOException {
            RandomAccessFile f = new RandomAccessFile(file, "r");
            byte[] b = new byte[(int) f.length()];
            f.readFully(b);
        return DigestUtils.sha256Hex(b);
    }
}
