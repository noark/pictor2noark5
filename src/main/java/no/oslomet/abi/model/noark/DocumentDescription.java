package no.oslomet.abi.model.noark;

import java.time.OffsetDateTime;
import java.util.Objects;

public class DocumentDescription
        extends NoarkEntity
        implements ITitleAndDescription {

    private String documentTypeCode;

    private String documentTypeCodeName;

    private String documentStatus;

    private OffsetDateTime associatedDate = OffsetDateTime.now();

    private String title;

    private String associatedBy;

    private String description;

    private String associatedWithRecordAsCode;

    private String associatedWithRecordAsCodeName;

    private Integer documentNumber;

    private OffsetDateTime associationDate;

    public String getDocumentTypeCodeName() {
        return documentTypeCodeName;
    }


    public String getAssociatedBy() {
        return associatedBy;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public String getDocumentStatus() {
        return documentStatus;
    }

    public OffsetDateTime getAssociatedDate() {
        return associatedDate;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssociatedWithRecordAsCodeName() {
        return associatedWithRecordAsCodeName;
    }


    public Integer getDocumentNumber() {
        return documentNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DocumentDescription that = (DocumentDescription) o;
        return Objects.equals(documentTypeCode, that.documentTypeCode) &&
                Objects.equals(documentTypeCodeName,
                        that.documentTypeCodeName) &&
                Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(associatedWithRecordAsCode,
                        that.associatedWithRecordAsCode) &&
                Objects.equals(associatedWithRecordAsCodeName,
                        that.associatedWithRecordAsCodeName) &&
                Objects.equals(documentNumber, that.documentNumber) &&
                Objects.equals(associationDate, that.associationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), documentTypeCode,
                documentTypeCodeName, title, description,
                associatedWithRecordAsCode, associatedWithRecordAsCodeName,
                documentNumber, associationDate);
    }
}
