package no.oslomet.abi.model.noark;

import java.util.Objects;

public class DocumentObject
        extends NoarkGeneralEntity {

    private Integer versionNumber;
    private String format;
    private String variantFormatCode;
    private String variantFormatCodeName;
    private String referenceDocumentFile;
    private String checksum;
    private String checksumAlgorithm;
    private Long fileSize;

    public Integer getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getVariantFormatCode() {
        return variantFormatCode;
    }

    public void setVariantFormatCode(String variantFormatCode) {
        this.variantFormatCode = variantFormatCode;
    }

    public String getVariantFormatCodeName() {
        return variantFormatCodeName;
    }

    public void setVariantFormatCodeName(String variantFormatCodeName) {
        this.variantFormatCodeName = variantFormatCodeName;
    }

    public String getReferenceDocumentFile() {
        return referenceDocumentFile;
    }

    public void setReferenceDocumentFile(String referenceDocumentFile) {
        this.referenceDocumentFile = referenceDocumentFile;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksumAlgorithm() {
        return checksumAlgorithm;
    }

    public void setChecksumAlgorithm(String checksumAlgorithm) {
        this.checksumAlgorithm = checksumAlgorithm;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        return "DocumentObject{" +
                "versionNumber=" + versionNumber +
                ", variantFormatCode='" + variantFormatCode + '\'' +
                ", variantFormatCodeName='" + variantFormatCodeName + '\'' +
                ", checksum='" + checksum + '\'' +
                ", checksumAlgorithm='" + checksumAlgorithm + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DocumentObject that = (DocumentObject) o;
        return Objects.equals(versionNumber, that.versionNumber) &&
                Objects.equals(variantFormatCode, that.variantFormatCode) &&
                Objects.equals(variantFormatCodeName,
                        that.variantFormatCodeName) &&
                Objects.equals(checksum, that.checksum) &&
                Objects.equals(checksumAlgorithm, that.checksumAlgorithm) &&
                Objects.equals(fileSize, that.fileSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), versionNumber,
                variantFormatCode, variantFormatCodeName, checksum,
                checksumAlgorithm, fileSize);
    }
}
