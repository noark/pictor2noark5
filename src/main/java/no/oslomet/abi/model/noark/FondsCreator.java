package no.oslomet.abi.model.noark;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class FondsCreator {

    /**
     * M006 - arkivskaperID code name (xs:string)
     */
    private String fondsCreatorId;

    /**
     * M023 - arkivskaperNavn code name (xs:string)
     */
    private String fondsCreatorName;

    /**
     * M021 - beskrivelse code name (xs:string)
     */
    private String description;

    public String getFondsCreatorId() {
        return fondsCreatorId;
    }

    public void setFondsCreatorId(String fondsCreatorId) {
        this.fondsCreatorId = fondsCreatorId;
    }

    public String getFondsCreatorName() {
        return fondsCreatorName;
    }

    public void setFondsCreatorName(String fondsCreatorName) {
        this.fondsCreatorName = fondsCreatorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "FondsCreator{" +
                "fondsCreatorId='" + fondsCreatorId + '\'' +
                ", fondsCreatorName='" + fondsCreatorName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        FondsCreator rhs = (FondsCreator) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(fondsCreatorId, rhs.fondsCreatorId)
                .append(fondsCreatorName, rhs.fondsCreatorName)
                .append(description, rhs.description)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(fondsCreatorId)
                .append(fondsCreatorName)
                .append(description)
                .toHashCode();
    }
}
