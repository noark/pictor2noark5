package no.oslomet.abi.model.noark;

import java.time.OffsetDateTime;

public interface IFinalised {
    String getFinalisedBy();
    OffsetDateTime getFinalisedDate();
}

