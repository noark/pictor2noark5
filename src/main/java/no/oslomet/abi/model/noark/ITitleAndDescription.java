package no.oslomet.abi.model.noark;

public interface ITitleAndDescription {
    String getTitle();
    void setTitle(String title);
    String getDescription();
    void setDescription(String description);
}

