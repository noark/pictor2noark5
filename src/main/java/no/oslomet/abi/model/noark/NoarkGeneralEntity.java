package no.oslomet.abi.model.noark;

import no.oslomet.abi.model.noark.utils.DateTimeAdapter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.OffsetDateTime;

public class NoarkGeneralEntity
        extends NoarkEntity
        implements IFinalised, ITitleAndDescription {

    /**
     * M020 - tittel (xs:string)
     */
    @XmlElement(name = "tittel", required = true)
    private String title;

    /**
     * M021 - beskrivelse (xs:string)
     */
    @XmlElement(name = "besrkivelse")
    private String description;

    /**
     * M602 - avsluttetDato (xs:dateTime)
     */
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    @XmlElement(name = "avsluttetDato", required = true)
    private OffsetDateTime finalisedDate = OffsetDateTime.now();

    /**
     * M603 - avsluttetAv (xs:string)
     */
    @XmlElement(name = "avsluttetAv", required = true)
    private String finalisedBy;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title != null && !title.isEmpty()) {
            this.title = title;
        } else
            this.title = "Ingen verdi, men verdi må være satt";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OffsetDateTime getFinalisedDate() {
        return finalisedDate;
    }

    public void setFinalisedDate(OffsetDateTime finalisedDate) {
        this.finalisedDate = finalisedDate;
    }

    public String getFinalisedBy() {
        return finalisedBy;
    }

    public void setFinalisedBy(String finalisedBy) {
        this.finalisedBy = finalisedBy;
    }

    public String toString() {

        return super.toString() +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", finalisedDate=" + finalisedDate +
                ", finalisedBy='" + finalisedBy + '\'';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        NoarkGeneralEntity rhs = (NoarkGeneralEntity) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(title, rhs.title)
                .append(description, rhs.description)
                .append(finalisedDate, rhs.finalisedDate)
                .append(finalisedBy, rhs.finalisedBy)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(title)
                .append(description)
                .append(finalisedDate)
                .append(finalisedBy)
                .toHashCode();
    }
}
