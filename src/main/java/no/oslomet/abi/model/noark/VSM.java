package no.oslomet.abi.model.noark;

import java.util.Objects;

public class VSM {

    private String arkivbildare;
    private String bildnummer;
    private Boolean negativ;
    private String plats;
    private String ar;
    private String motiv;
    private String publikation;
    private String bildrattigheter;
    private String anmarkningar;
    private String placering;
    private Integer index;

    public String getArkivbildare() {
        return arkivbildare;
    }

    public void setArkivbildare(String arkivbildare) {
        this.arkivbildare = arkivbildare;
    }

    public String getBildnummer() {
        return bildnummer;
    }

    public void setBildnummer(String bildnummer) {
        this.bildnummer = bildnummer;
    }

    public Boolean getNegativ() {
        return negativ;
    }

    public void setNegativ(Boolean negativ) {
        this.negativ = negativ;
    }

    public String getPlats() {
        return plats;
    }

    public void setPlats(String plats) {
        this.plats = plats;
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getMotiv() {
        return motiv;
    }

    public void setMotiv(String motiv) {
        this.motiv = motiv;
    }

    public String getPublikation() {
        return publikation;
    }

    public void setPublikation(String publikation) {
        this.publikation = publikation;
    }

    public String getBildrattigheter() {
        return bildrattigheter;
    }

    public void setBildrattigheter(String bildrattigheter) {
        this.bildrattigheter = bildrattigheter;
    }

    public String getAnmarkningar() {
        return anmarkningar;
    }

    public void setAnmarkningar(String anmarkningar) {
        this.anmarkningar = anmarkningar;
    }

    public String getPlacering() {
        return placering;
    }

    public void setPlacering(String placering) {
        this.placering = placering;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "VSM{" +
                "arkivbildare='" + arkivbildare + '\'' +
                ", negativ=" + negativ +
                ", plats='" + plats + '\'' +
                ", ar='" + ar + '\'' +
                ", motiv='" + motiv + '\'' +
                ", publikation='" + publikation + '\'' +
                ", bildrattigheter='" + bildrattigheter + '\'' +
                ", anmarkningar='" + anmarkningar + '\'' +
                ", placering='" + placering + '\'' +
                ", index=" + index +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VSM vsm = (VSM) o;
        return Objects.equals(arkivbildare, vsm.arkivbildare) &&
                Objects.equals(negativ, vsm.negativ) &&
                Objects.equals(plats, vsm.plats) &&
                Objects.equals(ar, vsm.ar) &&
                Objects.equals(motiv, vsm.motiv) &&
                Objects.equals(publikation, vsm.publikation) &&
                Objects.equals(bildrattigheter, vsm.bildrattigheter) &&
                Objects.equals(anmarkningar, vsm.anmarkningar) &&
                Objects.equals(placering, vsm.placering) &&
                Objects.equals(index, vsm.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(arkivbildare, negativ, plats, ar, motiv,
                publikation, bildrattigheter, anmarkningar, placering, index);
    }
}
